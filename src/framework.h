// Switch-DayNight
// © 2021 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

#pragma once

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>
#include <shellapi.h>


// Switch-DayNight
// © 2021 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

#include "framework.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR lpCmdLine,
	_In_ int nCmdShow) {
	UNREFERENCED_PARAMETER(hPrevInstance);

	BOOL succeeds;
	if (*lpCmdLine == L'\0') {
		succeeds = SendNotifyMessage(HWND_BROADCAST, WM_SYSCOLORCHANGE, 0, 0);
	} else {
		SHELLEXECUTEINFO sei;
		ZeroMemory(&sei, sizeof(SHELLEXECUTEINFO));
		sei.cbSize = sizeof(SHELLEXECUTEINFO);
		sei.lpVerb = L"open";
		sei.lpFile = L"node";
		sei.lpParameters = lpCmdLine;
		sei.nShow = SW_HIDE;
		succeeds = ShellExecuteEx(&sei);
	}

	return succeeds ? ERROR_SUCCESS : GetLastError();
}

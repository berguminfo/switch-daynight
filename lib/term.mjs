// Switch-DayNight
// © 2021 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

import createDebug from "debug";
const debug = createDebug("debug");
const info = createDebug("info");
const error = createDebug("error");
import { constants } from "fs";
import * as fs from "fs/promises";
import * as path from "path";
import { ColorSchemes } from "../daynight.config.mjs";

const SettingsFiles = [
    path.join(process.env.LOCALAPPDATA, "Packages", "Microsoft.WindowsTerminal_8wekyb3d8bbwe", "LocalState", "settings.json"),
    path.join(process.env.LOCALAPPDATA, "Packages", "Microsoft.WindowsTerminalPreview_8wekyb3d8bbwe", "LocalState", "settings.json")
];

export default async function(scheme) {
    if (process.platform !== "win32") return;

    const promises = SettingsFiles.map(async fileName => {

        // check access
        try {
            await fs.access(fileName, constants.R_OK | constants.W_OK);
        } catch {
            info(`[term] not found: '${fileName}'`);
            return;
        }

        try {

            // parse and update JSON
            // NOTE: touches only profiles having custom colorScheme 
            const settings = JSON.parse(await fs.readFile(fileName, "utf-8"));
            const list = settings.profiles && settings.profiles.list || [];
            const colorScheme = ColorSchemes.term[scheme];
            let modified = false;
            for (const profile of list) {
                if (profile.colorScheme) {
                    if (profile.colorScheme === colorScheme) {
                        debug(`[term] skip change profile: '${profile.name}'='${colorScheme}'`);
                    } else {
                        profile.colorScheme = colorScheme;
                        modified = true;
                    }
                }
            }
            
            // save settings
            if (modified) {
                await fs.writeFile(fileName, JSON.stringify(settings, null, 2));
                info(`[term] change profiles: colorScheme='${colorScheme}'`);
            }
        } catch (err) {
            error("[term]", err);
        }
    });

    await Promise.all(promises);
}
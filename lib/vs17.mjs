// Switch-DayNight
// © 2021 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

import createDebug from "debug";
const debug = createDebug("debug");
const info = createDebug("info");
const error = createDebug("error");
import { exec } from "child_process";
import { constants } from "fs";
import * as fs from "fs/promises";
import * as path from "path";
import { fileURLToPath } from 'url';
import { DOMParser, XMLSerializer } from "xmldom";
import * as xpath from "xpath";
import { ColorSchemes } from "../daynight.config.mjs";

const SDN_PATH = process.env.SDN_PATH || path.join("..", "bin", process.arch, "sdn.exe");
const THEME_XPATH = "//Category[@RegisteredName = 'Environment_Group']/Category[@RegisteredName = 'Environment_FontsAndColors']/FontsAndColors/Theme/@Id";

const CurrentSettings = {
    prefix: path.join(process.env.LOCALAPPDATA, "Microsoft", "VisualStudio"),
    version: /^17\.[_\d\w]+$/gi,
    fileName: path.join("Settings", "CurrentSettings.vssettings")
};

export default async function(scheme) {
    if (process.platform !== "win32") return;

    try {

        // find all Visual Studio versions installed
        const dirents = await fs.readdir(CurrentSettings.prefix, { withFileTypes: true });
        const themeId = ColorSchemes.vs17[scheme];
        dirents.filter(element => {
            return element.isDirectory() && CurrentSettings.version.test(element.name);
        }).forEach(async element => {

            // check access
            const fileName = path.join(CurrentSettings.prefix, element.name, CurrentSettings.fileName);
            try {
                await fs.access(fileName, constants.R_OK | constants.W_OK);
            } catch {
                info(`[vs17] not found: '${fileName}'`);
                return;
            }

            // parse and update XML
            try {
                const xml = await fs.readFile(fileName, "utf-8");
                const document = new DOMParser().parseFromString(xml);
                const themeNode = xpath.select(THEME_XPATH, document)[0];
                if (themeNode) {
                    if (themeNode.value === themeId) {
                        debug(`[vs17] skip change theme@Id: ${themeNode.value}`);
                    } else {
                        themeNode.value = themeId;

                        // save and send notification to Visual Studio
                        await fs.writeFile(fileName, new XMLSerializer().serializeToString(document));
                        info(`[vs17] change theme@Id: ${themeNode.value}`);

                        const basename = path.dirname(fileURLToPath(import.meta.url));
                        const command = path.join(basename, SDN_PATH);
                        await fs.access(command, constants.X_OK);
                        exec(command);
                        info(`[vs17] execute: '${command}'`);
                    }
                }
            } catch (err) {
                error("[vs17]", err);
            }
        });
    } catch (err) {
        error("[vs17]", err);
    }
}
// Switch-DayNight
// © 2021 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

import createDebug from "debug";
const debug = createDebug("debug");
const info = createDebug("info");
import Registry from "winreg";

const PersonalizeKeys = [
    "AppsUseLightTheme",
    "SystemUsesLightTheme"
];

export default async function(scheme) {
    if (process.platform !== "win32") return;

    const personalizeKey = new Registry({
        hive: Registry.HKCU,
        key: "\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize"
    });
    const personalizeValue = scheme !== 1 ? 1 : 0;

    // update themes
    for (const key of PersonalizeKeys) {
        personalizeKey.get(key, (err, result) => {
            if (err) throw err;

            if (result.value === `0x${personalizeValue}`) {
                debug(`[winrt] skip change light theme: ${key}=${personalizeValue}`);
            } else {
                personalizeKey.set(key, Registry.REG_DWORD, personalizeValue, err => {
                    if (err) throw err;
                });
                info(`[winrt] change light theme: ${key}=${personalizeValue}`);
            }
        });
    }
}
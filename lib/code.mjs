// Switch-DayNight
// © 2021 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

import createDebug from "debug";
const debug = createDebug("debug");
const info = createDebug("info");
const error = createDebug("error");
import { constants } from "fs";
import * as fs from "fs/promises";
import * as path from "path";
import { ColorSchemes } from "../daynight.config.mjs";

const SettingsFiles = [
    process.platform === "win32" ?
        path.join(process.env.APPDATA, "Code", "User", "settings.json") :
    (process.platform === "linux" ?
        path.join(process.env.HOME, ".config", "Code", "User", "settings.json") :
    (process.platform === "darwin" ?
        path.join(process.env.HOME, "Library", "Application Support", "Code", "User", "settings.json") :
    ""))
];

export default async function(scheme) {
    const promises = SettingsFiles.map(async fileName => {

        // check access
        try {
            await fs.access(fileName, constants.R_OK | constants.W_OK);
        } catch {
            info(`[code] not found: '${fileName}'`);
            return;
        }

        try {

            // parse and update JSON
            const settings = JSON.parse(await fs.readFile(fileName, "utf-8"));
            const colorTheme = ColorSchemes.code[scheme];
            let modified = false;
            if (settings["workbench.colorTheme"]) {
                if (settings["workbench.colorTheme"] === colorTheme) {
                    debug(`[code] skip change theme: workbench.colorTheme='${colorTheme}'`);
                } else {
                    settings["workbench.colorTheme"] = colorTheme;
                    modified = true;
                }
            }

            // save settings
            if (modified) {
                await fs.writeFile(fileName, JSON.stringify(settings, null, 2));
                info(`[code] change theme: workbench.colorTheme='${colorTheme}'`);
            }
        } catch (err) {
            error("[code]", err);
        }
    });

    await Promise.all(promises);
}
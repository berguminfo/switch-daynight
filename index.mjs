// Switch-DayNight
// © 2021 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

import createDebug from "debug";
const debug = createDebug("debug");
import { createSun } from "astronomy-bundle/sun/index.js";
import { createTimeOfInterest } from "astronomy-bundle/time/index.js";
import { default as updateWinrt } from "./lib/winrt.mjs";
import { default as updateVs17 } from "./lib/vs17.mjs";
import { default as updateCode } from "./lib/code.mjs";
import { default as updateTerm } from "./lib/term.mjs";
import {
    Geoposition,
    STANDARD_ALTITUDE,
    DefaultInclude,
    DefaultExclude,
    LIGHT_SCHEME,
    DARK_SCHEME,
    RUNTIME_APP,
    VISUAL_STUDIO_17,
    VISUAL_STUDIO_CODE,
    WINDOWS_TERMINAL
} from "./daynight.config.mjs";

const Handlers = new Map([
    [RUNTIME_APP, scheme => updateWinrt(scheme)],
    [VISUAL_STUDIO_17, scheme => updateVs17(scheme)],
    [VISUAL_STUDIO_CODE, scheme => updateCode(scheme)],
    [WINDOWS_TERMINAL, scheme => updateTerm(scheme)]
]);

async function getColorScheme(options, knownSchemes = ["-light", "-dark", "-highcontrast"]) {

    // color scheme by options
    for (const option of options) {
        const scheme = knownSchemes.findIndex(element => element.startsWith(option));
        if (scheme >= 0) return scheme;
    }

    // color scheme by sun rise/set
    const now = new Date();
    const celestialBody = createSun(createTimeOfInterest.fromDate(now));
    try {
        const rise = (await celestialBody.getRise(Geoposition, STANDARD_ALTITUDE)).getDate();
        const set = (await celestialBody.getSet(Geoposition, STANDARD_ALTITUDE)).getDate();
        debug(`rise: ${rise.toLocaleTimeString()}, set: ${set.toLocaleTimeString()}, altitude: ${STANDARD_ALTITUDE}°`);

        const riseTime = rise.getTime();
        const nowTime = now.getTime();
        const setTime = set.getTime();
        return riseTime < nowTime && nowTime < setTime ? LIGHT_SCHEME : DARK_SCHEME;
    } catch {
        const geoAppEquCoords = await celestialBody.getApparentGeocentricEquatorialSphericalCoordinates();
        return geoAppEquCoords.declination >= 0 ? LIGHT_SCHEME : DARK_SCHEME;
    }
}

async function update(scheme, include, exclude) {

    // include/exclude
    exclude.forEach(excludeElement => {
        const index = include.findIndex(includeElement => includeElement && includeElement.startsWith(excludeElement));
        if (index >= 0) include[index] = null;
    });
    // NOTE: keep include duplicates, it's the operator who dictates!! 
    const promises = include.map(element => {
        if (element != null && Handlers.has(element)) {
            return Handlers.get(element)(scheme);
        }
    });

    await Promise.all(promises);
}

function stringToArray(prefix, options) {
    const regexp = new RegExp(`^${prefix}[^:]*:(.+)$`, "i");
    for (const option of options) {
        const match = option.match(regexp);
        if (match) {
            return match[1].split(",");
        }
    }
}

const options = process.argv.slice(2).map(element => element.toLowerCase());
const include = stringToArray("-i", options);
const exclude = stringToArray("-e", options);
getColorScheme(options).then(scheme =>
    update(scheme, include || DefaultInclude, exclude || DefaultExclude));
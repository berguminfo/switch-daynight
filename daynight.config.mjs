// Switch-DayNight
// © 2021 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

import { STANDARD_ALTITUDE_SUN_CIVIL_TWILIGHT } from "astronomy-bundle/constants/standardAltitude.js";

// Required. Keep as is.
export const RUNTIME_APP = "winrt";
export const VISUAL_STUDIO_17 = "vs17";
export const VISUAL_STUDIO_CODE = "code";
export const WINDOWS_TERMINAL = "term";

// Set geoposition
export const Geoposition = { lat: TODO, lon: TODO };

// Increase or decrease to change rise/set time
export const STANDARD_ALTITUDE = STANDARD_ALTITUDE_SUN_CIVIL_TWILIGHT * -1;

// Default include/exclude
export const DefaultInclude = [VISUAL_STUDIO_CODE, WINDOWS_TERMINAL, VISUAL_STUDIO_17, RUNTIME_APP];
export const DefaultExclude = [];

export const ColorSchemes = {
    code: [
        "Winter is Coming (Light - No Italics)",
        "Winter is Coming (Dark Blue - No Italics)",
        "Default High Contrast"
    ],
    term: [
        "One Half Light",
        "Campbell",
        "Tango Light"
    ],
    // See "$($env:DevEnvDir)Common7\IDE\CommonExtensions\Platform" *.pkgdef files.
    vs17: [
        "{A4D6A176-B948-4B29-8C66-53C97A1ED7D0}", // Blue
        "{D06E164F-FC85-41DE-832E-2C5819C4B31B}", // Winter is Comming
        "{A5C004B4-2D4B-494E-BF01-45FC492522C7}"  // HighContrast
    ]
};

// Keep in sync with ColorSchemes ordering
export const LIGHT_SCHEME = 0;
export const DARK_SCHEME = 1;
export const HICONTRAST_SCHEME = 2;
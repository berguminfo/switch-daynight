if (Get-Module Switch-DayNight) { return }

# Call node index.js with safe parameters
function Switch-DayNight {
    Param(
        # Select color scheme
        [switch] $Light,
        [switch] $Dark,
        [switch] $HighContrast,
        # Items to include/exclude
        [string[]] $Include,
        [string[]] $Exclude,
        # Include shortcuts
        [switch] $Code,
        [switch] $Term,
        [switch] $Vs17,
        [switch] $Winrt
    )
    if ($Code) { $Include += "code" }
    if ($Term) { $Include += "term" }
    if ($Vs17) { $Include += "vs17" }
    if ($Winrt) { $Include += "winrt" }

    $Expressions = ("node", "$PSScriptRoot\index.mjs")
    if ($Light) { $Expressions += "-l" }
    if ($Dark) { $Expressions += "-d" }
    if ($HighContrast) { $Expressions += "-h" }
    if ($Include) { $Expressions += "-i:$($Include -join ',')" }
    if ($Exclude) { $Expressions += "-e:$($Exclude -join ',')" }
 
    Invoke-Expression ($Expressions -join " ")
}

Export-ModuleMember -Function Switch-DayNight
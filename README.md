# switch-daynight

switch-daynight - switch day/night color scheme

## Features

- `code`
  - : Switch Visual Studio Code theme.
- `term`
  - : Switch Windows Terminal profile scheme.
- `vs17`
  - : Switch Visual Studio 2022+ theme.
  - : Try notify theme change.
- `winrt`
  - : Switch WinRT Applications theme.

## How to Use

Start the `Developer Command Prompt for VS 2022`.

```sh
yarn install
yarn run build:Release
node index.mjs [options]
```

### Options

Any option can be in any case and/or truncated.
Hence `-L` equals `-LIGHT` or `-light`.

#### Color scheme options:
- `-Light`
  - : Switch to the Light scheme.
- `-Dark`
  - : Switch to the Dark scheme.
- `-HighContrast`
  - : Switch to the HighContrast scheme.

#### Features options:
- `-Include`
  - : Features to include.
- `-Exclude`
  - : Features to exclude.

#### Additional PowerShell options:
- `-code`
  - : Include Visual Studio Code.
- `-term`
  - : Include Windows Terminal.
- `-vs17`
  - : Include Visual Studio 2022+.
- `-winrt`
  - : Include WinRT Applications.

Without any color scheme option the scheme will be selected given the sun rise/set time at the configurated geoposition.

## Configuration

Open `daynight.config.mjs`.

The `Geolocation` object specifies the location to find sun rise/set times.

The `ColorSchemes` object specifies the color schemes to use for each feature.

## PowerShell

```powershell
Import-Module "\path\to\switch-daynight\Switch-DayNight"
Set-Alias sdn Switch-DayNight
help sdn
# execute with:
.\Switch-DayNight.ps1
```

## sdn.exe

The `binx\x64\sdn.exe` executable can be used to run `index.mjs` without the console window to open.
```powershell
bin\x64\sdn.exe index.mjs [options]
```

## Bugs

- `vs17`
  - : `Visual Studio 2022` don't respond to `WM_SYSCOLORCHANGE` messages anymore. Hence configuration changes takes no effect. Then closing, any previous configurations will be overwritten. Hence `Visual Studio` MUST be in the closed state before attempting to switch day/night scheme.